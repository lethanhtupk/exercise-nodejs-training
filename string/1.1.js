class MyRegExp extends RegExp {
    [Symbol.matchAll](str) {
        let result = RegExp.prototype[Symbol.matchAll].call(this, str);
        if (!result) {
            return null;
        }
        return Array.from(result);
    }
}

function matchPattern(str) {
    let re = new MyRegExp('nhà', 'ugi');
    let arr = str.matchAll(re);
    return arr;
}


str = "ngôi nhà của em là một căn nhà hai tầng thoáng mát và rộng rãi. Nhìn từ xa là có thể thấy tường nhà màu vàng chanh ấm áp cùng giàn hoa giấy nở rộ trên tường nhà. Tô điểm xung quanh là những cây xanh rợp bóng mát khiến cho ngôi nhà trở nên hài hòa và đẹp vô cùng. Chiếc cổng sắt lớn đã bảo vệ an toàn cho ngôi nhà này suốt bao nhiêu năm qua. Tiến qua cánh cổng là một cái sân lớn được lát gạch đỏ. Ở hai bên sân là những chậu cây cảnh, những chậu hoa với đủ màu sắc và đủ loại luôn được mẹ em chăm sóc cẩn thận."
let arr = matchPattern(str)
console.log(arr.length)
console.log(arr[0].index)
console.log(arr[arr.length-1].index)