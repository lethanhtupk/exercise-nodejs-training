function checkJSON(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

console.log(checkJSON('{ "name":"John", "age":30, city:"New York"}'))