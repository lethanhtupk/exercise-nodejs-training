class Student {
    constructor(name, age, gender, average) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.average = average;
    }
}

module.exports = Student;