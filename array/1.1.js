import Student from './student';

function count(students) {
    male_num = 0;
    female_num = 0;
    students.forEach(student => {
        if (student.gender === "male") {
            male_num += 1;
        } 
        else {
            female_num += 1;
        } 
    });

    return [male_num, female_num];
}