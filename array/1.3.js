import Student from './student';


// ham kiem tra co hoc sinh nu nao hoc luc yeu?
// su dung ham some()
function checkgirl(students) {
    return students.some(student => {
        return (student.gender === "female" && student.average < 2)
    })
}

// tra ve hoc sinh nam hoc luc yeu dau tien
function check(student) {
    return (student.gender === "male" && student.average < 2);
}

// kiem tra co hoc sinh nam hoc luc yeu ko? neu co tra ve true
// su dung ham find()
function checkboy(students) {
    if (students.find(check) != undefined) {
        return true;
    }
    return false;
}

// ham sap xep hoc sinh trong mang C theo thu tu cao den thap 
function sortStudent(students) {
    return students.sort(function (a,b) {
        return b.average-a.average;
    })
}

// lay 10 hoc sinh gioi nhat
function getTopTen(students) {
    sorted_student = sortStudent(students);
    top_ten = sorted_student.slice(0,10);
    return top_ten.map(student => student.name).join(",");
}


// dung ham filter de lay ra mang hoc sinh gioi truoc
// gia su mang students dau vao la mang hoc sinh gioi
function getExcellentStudent(students) {
    students.map(student => {
        return {
            name: student.name,
            average: student.average
        }
    })
}
