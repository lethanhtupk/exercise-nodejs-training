function convert(str){
    day = str.slice(0,2);
    month = str.slice(3,5);
    year = str.slice(-4);
    return new Date(year, month-1, day);
    
}

console.log(convert("19-10-2019"))