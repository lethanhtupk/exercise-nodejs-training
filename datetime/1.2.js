function getTime() {
    let input = window.prompt("Enter input");
    if(input != null) {
        let today = new Date();
        let date = today.getDate() + "/" + (today.getMonth()+1) + "/" + today.getFullYear();
        let time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        let dateTime = date + " " + time;
        return dateTime
    }
}

console.log(getTime())